
// Using FETCH to retrieve all the items from JSON Placeholder API
	
	fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => {

		let list = json.map((todo => {
			return todo.title;
		}))

		console.log(list);
	});


	fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then((response) => response.json())
	.then((json) => console.log(json));

// FETCH request using the GET method that retrieves a single item from JSON Placeholder API

	fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then((response) => response.json())
	.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));


// FETCH request using the POST method that creates an item from JSON Placeholder API

	fetch("https://jsonplaceholder.typicode.com/todos", {
		method: "POST",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Created To Do List Item",
			completed: false,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


// FETCH request using the PUT method that updates an item from JSON Placeholder API

	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PUT",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Updated To Do List Item ",
			description: "To update the my to do list with a different data structures",
			userId: 1,
			status: "Pending",
			dateCompleted: "Pending"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


// FETCH request using the PATCH method that updates the status of a item from JSON Placeholder API

	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			status: "Completed",
			dateCompleted: "07/09/21"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


// DELETE AN ITEM

	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE",
	})