// console.log("Hello World");


// Javascript Synchronous and Asynchronous
	/*
		Synchronous
			- Synchronous codes runs in sequence. This means that each operation must wait for the previous one to complete before executing.

		Asynchronous
			- Asynchronous code runs in parallel. This means that an operation can occur while another one is still being processed.

			- Asynchronous code execution is often preferrable in situations where execution can be blocked. Some examples of this are netwrok requests, long-running calculations, file system operations, etc. Using asynchronous code in the browser ensures the page remains response and the user experience is mostly unaffected.
	*/

		// Example for Synchronous:
			console.log("Hello World");
			// cosnole.log("Hello Again");
			
			// for (let i = 0; i <= 10; i++) {
			// 	console.log(i);
			// }

			console.log("Hello It's me");


// API (Application Programming Interface)
	/*
		- API stands for Application Programming Interface
		- An application programming interface is a particular set of codes that allows software programs to communicate with each other
		- An API is the interface through which you can access someone else's code or through which someone else's code accesses yours.
	*/

		// Example:
			/*
				Google APIs
					https://developers.google.com/identity/sign-in/web/sign-in

				Youtube APIs
					https://developers.google.com/youtube/iframe_api_reference
			*/


// Fetch API

	console.log(fetch("https://jsonplaceholder.typicode.com/posts"))
		/*
			- A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

			- A promise is in one of these three states:
				Pending:
					- Initial state, neither fulfilled nor rejected
				Fulfilled:
					- Operation was successfully completed
				Rejected:
					- Operation failed
		*/

	fetch("https://jsonplaceholder.typicode.com/posts")
	.then(response => console.log(response.status))
		/*
			- By using the .then method, we can now check for the status of the promise
			- The "fetch" method will return a "promise" that resolves to be a "response" object
			- The ".then" method captures the  "response" object and returns another "promise" which will eventually be "resolved" or "rejected"

			Syntax:
				fetch("URL")
				.then((response) => {})
		*/

	fetch("https://jsonplaceholder.typicode.com/posts")
	// Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in ourr application
	.then((response) => response.json())
	// Print the converted JSON value from the  "fetch" request
	// Using multiple ".then" method to create a promise chain
	.then((json) => console.log(json))


// Async and Await
	/*
		- The "async" and "await" keywords is a another approach that can be used to achieve asynchronous codes
		- Used in functions to indicate which portions of code should be waited
		- Creates an asynchronous function
	*/

		async function fetchData () {
			// Waits for the "fetch "method to complete then stores the value in the "result" variable
			let result = await fetch("https://jsonplaceholder.typicode.com/posts")
			// Results returned by fetch is returned as a promise
			console.log(result);
			// The returned "response" is an object
			console.log(typeof result);
			// We cannot access the content of the "response" by directly accessing it's body property
			console.log(result.body);

			let json = await result.json();
			console.log(json);
		};
		fetchData();

		
// Getting a specific post

	// Retrieves a specific post following the REST API (retrieve, /posts/id:, GET)

		fetch("https://jsonplaceholder.typicode.com/posts/1")
		.then((response) => response.json())
		.then((json) => console.log(json));

	/*
		Postman

		url:https://jsonplaceholder.typicode.com/posts/1
		method: GET
	*/
	

// Creating a Post
	// Creates a new post following the REST API (create, /posts/id, POST)
		/*
			Syntax:
				fetch("URL, options")
				.then((response) => {})
				.then((response) => {})
		*/
	
			fetch("https://jsonplaceholder.typicode.com/posts", {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					title: "New post",
					body: "Hello World",
					userId: 1
				})
			})
			.then((response) => response.json())
			.then((json) => console.log(json));

			/*
				POSTMAN
				url: https://jsonplaceholder.typicode.com/posts
				method: POST
				body: raw + JSON
					{
						"title": "New post",
						"body": "Hello World",
						"userId": 1
					}
			*/


// Updating a Post
	// Updates a specific post following the REST API (update, /posts/id, PUT)

		fetch("https://jsonplaceholder.typicode.com/posts/1", {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				id: 1,
				title: "update post",
				body: "Hello Again",
				userId: 1
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

		/*
			POSTMAN
			url: https://jsonplaceholder.typicode.com/posts/1
			method: PUT
			body: raw + JSON
				{
					"title": "My First Revised Blog Post",
					"body": "Hello there! I revised this a bit",
					"userId": 1
				}
		*/


// Update a Post using PATCH method
	/*
		- Updates a specific post following the REST API (updates, /posts/id, PATCH)
		- The differene between PUT and PATCH is the number of properties being changed
		- PATCH updates the parts
		- PUT updates the whole documents
	*/

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
			method: "PATCH",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: "Corrected post"
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

		/*
			POSTMAN
			url: https://jsonplaceholder.typicode.com/posts/1
			method: PATCH
			body: raw + JSON
				{
					"title": "This is my final title"
				}
		*/


// Deleting a Post
	// Deleting a specific post following the REST API (delete, /posts/id, DELETE)

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "DELETE",
	})

		/*
			POSTMAN
			url: https://jsonplaceholder.typicode.com/posts/1
			method: DELETE
		*/


// Filtering the Post
	/*
		- The data can be filtered by sending the userId along with the URL
		- Information sent via the URL can be done by adding the question mark symbol (?)

		Syntax:
			Individual Parameters:
				'url?parameterName=value'
			Multiple Parameters:
				'url?paramA=valueA&paramB=valueB'
	*/


	fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
	.then((response) => response.json())
	.then((json) => console.log(json));

	fetch("https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3")
	.then((response) => response.json())
	.then((json) => console.log(json));


// Retrieve comments of a specific post
	// Retrieving comments for a specific post following the REST API (retrieve, /posts/id, GET)

	fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
	.then((response) => response.json())
	.then((json) => console.log(json));
